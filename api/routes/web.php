<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group( ['prefix' => 'app'], function (  ) {

  Route::group( ['prefix' => 'user'], function (  ) {

    Route::get ( '', ['user' => 'UserController@allUser'] );

    Route::get ( '{id}', [ 'user' => 'UserController@getUser' ] );

    Route::post ( '', ['user' => 'UserController@saveUser'] );

    Route::put ( '{id}', [ 'user' => 'UserController@updateUser' ] );

    Route::delete ( '{id}', [ 'user' => 'UserController@deleteUser' ] );
  } );
} );


// Route::get('/test', function () {
//     return 'test';
// });

// Route::get('/', function () {
//     return view('welcome');
// });
