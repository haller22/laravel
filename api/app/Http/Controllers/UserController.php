<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{

   protected $user = null;

    public function __construct ( User $user ) 
    {

      $this->user = $user;
    }

    public function allUser (  )
    {

      return $this->user->allUsers(  );
      // return $this->user;
    }    
    
    public function getUser ( $id )
    {

      return 'retorna usuario com ide' . $id;
    }

    public function saveUser (  )
    {

      return 'add usuario';
    }

    public function updateUser ( $id )
    {

      return 'atualiza usuario com id ' . $id;
    }

    public function deleteUser ( $id )
    {

      return 'remove usuario com id ' . $id;
    }
}
