<?php

class User extends Eloquent
{

    protected $hidden = ['passwd'];

    protected $fillable = [
        'frist_name',
        'last_name',
        'email',
        'passwd'
    ];

    public function allUsers (  )
    {

        return self::all(  );
    }

    public function save (  )
    {

        $input = Input::all(  );
        $user = new User (  );
        $user->fill($input);
        $user->save(  );
        return $user;
    }

}
