#!/bin/bash

SERVER_NAME='servo'
DATABASE_NAME='mariadb'

sudo docker-compose up -d

chown -R 1001:1001 /etc/mariadb 2> /dev/null

sudo docker logs -f $DATABASE_NAME
